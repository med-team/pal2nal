Source: pal2nal
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Pjotr Prins <pjotr.debian@thebird.nl>,
           Steffen Moeller <moeller@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/pal2nal
Vcs-Git: https://salsa.debian.org/med-team/pal2nal.git
Homepage: http://www.bork.embl.de/pal2nal
Rules-Requires-Root: no

Package: pal2nal
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: paml
Description: converts proteins to genomic DNA alignment
 PAL2NAL is a program that converts a multiple sequence alignment
 of proteins and the corresponding DNA (or mRNA) sequences into
 a codon-based DNA alignment. The program automatically assigns
 the corresponding codon sequence even if the input DNA sequence
 has mismatches with the input protein sequence, or contains UTRs,
 polyA tails. It can also deal with frame shifts in the input
 alignment, which is suitable for the analysis of pseudogenes.
 The resulting codon-based DNA alignment can further be subjected
 to the calculation of synonymous (Ks) and non-synonymous (Ka)
 substitution rates.
